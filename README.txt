Git Hook
--------

DEVELOPING for Git Hook
-----------------------

Any Git event that invokes a Git hook can be hooked into wit Drush hooks. These work the same as Drupal hooks although
when Drupal is not bootstrapped, module's implementations of these hooks cannot be seen or utilised.

See githooks.api.php for examples.

FAQ
---

How do I skip the validation and just commit my work?

  If you need to commit your work, and do not care to have it checked for errors,
  commit your code with:

  $ git commit --no-verify

How do I debug my githook action implementation?

  Alter or create a custom templates file and add the --debug command to the drush callback. Then re-install githooks.

