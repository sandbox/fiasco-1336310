<?php
/**
 * @file
 *  Drush commands to facilitate git event workflows in Drupal development.
 */

define('GITHOOK_API_VERSION', 2);

/**
 * Implementation of hook_drush_command().
 */
function githooks_drush_command() {
  $items = array();

  $items['githooks-install'] = array(
    'description' => 'Install & configure drush control of local git repository hooks.',
    'arguments' => array(
      'repo_dir' => 'Filepath to Git repository location. Repository maybe bare or clone.'
    ),
    'options' => array(
      'template' => "Select a file for use as the template for git script replacement. See 'resources/default.hook_template.php' for an example.",
      'event-blacklist' => 'Do not install githooks wrappers for a comma or space-delimited list of git hooks. If existing event scripts are found, keep them live.',
      'bootstrap-root' => 'The Drupal root. If present, drush will bootstrap this Drupal site before running any hooks.'
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['githooks-uninstall'] = array(
    'description' => 'Uninstall git hook integration and restore original hooks.',
    'arguments' => array(
      'repo_dir' => 'Filepath to Git repository location. Repository maybe bare or clone.'
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['githooks-event'] = array(
    'description' => 'React to triggered Git events and test workflows.',
    'arguments' => array(
      'event' => 'Specify which git event to trigger.',
    //  'arguments' => 'Arguments passed on by Git for the specific event.'
    ),
    //'options' => array(
    //  'root' => 'If specified, drush will attempt to fully bootstrap before invoking the event hook so that Drupal may respond to the event.',
    //),
    'examples' => array(
      'drush githooks-event pre-commit' => 'A git commit is about to happen, react or reject the commit.',
      'drush githooks-event post-commit' => 'Signal that a git commit just completed.',
      // Fill in examples for each event to document them.
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  return $items;
}

/**
 * Implementaton of hook_drush_help().
 */
function githooks_drush_help($section) {
  switch ($section) {
    case 'drush:githooks-install':
      return dt('Install & configure drush control of local git repository hooks. To learn more about hooks, type `git help hooks`.');
    case 'drush:githooks-uninstall':
      return dt('Uninstall drush control of local git repository hooks. To learn more about hooks, type `git help hooks`.');
    case 'drush:githooks-event':
      return dt('React to triggered Git events and test workflows.');
  }
}

/**
 * Validate that drush is being invoked from within a git repo.
 */
function githooks_validate_repo($location) {
  if (!drush_shell_exec('export GIT_DIR=%s && git rev-parse --git-dir', $location)) {
    return drush_set_error(__FUNCTION__, dt('%location is not a Git repository. To make this directory into the root of a new git repository, run `git init --bare %location; git add .; git commit -m "Initial commit."`', array('%location' => $location)));
  }
  return realpath(array_shift(drush_shell_exec_output()));
}

/**
 * Validation hook for drush_githooks_install().
 */
function drush_githooks_install_validate($location) {
  if (!isset($location)) {
    return drush_set_error(__FUNCTION__, "Location must be present");
  }
  if (!githooks_validate_repo($location)) {
    return drush_set_error(__FUNCTION__, "Bad location, not a repository.");
  }
}

/**
 * Implementation of drush_COMMAND() for githooks-event.
 */
function drush_githooks_install($location) {
  $location = githooks_validate_repo($location);

  $backup = TRUE;
  if (file_exists($location . '/hooks/GITHOOK_VERSION.txt')) {
    $version_info = file_get_contents($location . '/hooks/GITHOOK_VERSION.txt');
    if ($version_info == GITHOOK_API_VERSION) {
      return drush_set_error('GITHOOK_ALREADY_INSTALLED', dt('Githook is already properly installed in this repository.'));
    }
    else {
      drush_log('An older version of githooks has been installed. Updating...', 'notice');
      $backup = FALSE;
    }
  }

  // Backup original hooks.
  if ($backup && !githooks_backup($location)) {
    return drush_set_error(__FUNCTION__, "Failed to backup existing hooks");
  }

  // Overwrite hooks directory.
  if (githooks_install($location)) {
    drush_log(dt('Successfully installed githooks to !repo', array('!repo' => $location)), 'success');
  }
  else {
    githooks_restore($location);
    return drush_set_error(__FUNCTION__, "Githooks failed to install.");
  }
}

/**
 * Validation hook for drush_githooks_install().
 */
function drush_githooks_uninstall_validate($location) {
  if (!isset($location)) {
    return drush_set_error(__FUNCTION__, "Location must be present");
  }
  if (!githooks_validate_repo($location)) {
    return drush_set_error(__FUNCTION__, "Bad location, not a repository.");
  }
}

/**
 * Implementation of drush_COMMAND() for githooks-event.
 */
function drush_githooks_uninstall($location) {
  $location = githooks_validate_repo($location);

  if (file_exists($location . '/hooks/GITHOOK_VERSION.txt') && githooks_restore($location)) {
    drush_log('Successfully reverted to original git hooks directory. Drush is no longer involved in this repository.', 'success');
  }
  else {
    return drush_set_error(__FUNCTION__, "Drush hooks are either not installed or restoration failed.");
  }
}

/**
 * Implementation of drush_COMMAND() for githooks-event.
 */
function drush_githooks_event($event) {
  if (isset($event)) {
    drush_log(dt('githooks-event triggered with !event', array('!event' => $event)), 'notice');
  }

  if (!githooks_validate_event($event)) {
    foreach (githooks_events() as $git_event) {
      drush_print($git_event, 2);
    }
    return drush_set_error(__FUNCTION__, dt('You must specify a valid git event.'));
  }

  // Bootstrap Drupal if possible.
  if (drush_get_option(array('r', 'root'), FALSE)) {
    drush_bootstrap_max();
  }

  $hook = 'githooks_event_' . str_replace('-', '_', $event);

  $arguments = func_get_args();
  array_shift($arguments);

  $log = "Githooks event: $event";
  if (!empty($arguments)) {
    $log .= ' ("' . implode('", "', $arguments) . '")';
  }
  drush_log($log, 'status');
  array_unshift($arguments, $hook);


  return call_user_func_array('drush_command_invoke_all', $arguments);
}

/**
 * Validate that a specified term is a git event.
 */
function githooks_validate_event($event) {
  return array_key_exists($event, githooks_events());
}

/**
 * Set up files that point git events to launch drush.
 */
function githooks_install($repo) {
  $hooks_directory = $repo . "/hooks";

  $template = drush_get_option('template', dirname(__FILE__) . '/resources/default.hook_template.php');

  if (!file_exists($template)) {
    return drush_set_error("$template does not exist (--template).");
  }

  // If code is ever "1", a file operation failed.
  $code = 0;
  foreach (githooks_events() as $event) {
    $file = $hooks_directory . "/$event";
    $code |= drush_op_system("cp $template $file");
    $code |= drush_op_system("chmod +x $file");   
  }

  // Tag the githooks version in the hooks directory.
  $code |= drush_op_system('echo "' . GITHOOK_API_VERSION . '" > ' . $hooks_directory . '/GITHOOK_VERSION.txt');

  // Add a brief explanation of what's going on in the directory.
  $readme = "This directory is under the management of the Drush shell script to facilitate Drupal development.\n\n"
          . "To revert it to it's state before drush took over, please run \`drush githooks --uninstall\`\n"
          . "from the command line within the git directory.\n\n"
          . "Please do not change, delete, or move the GITHOOK_VERSION.txt file, as it is used to identify installation status.";

  $file = $hooks_directory . '/README.txt';
  $code |= drush_op_system("echo \"$readme\" > $file");

  if ($code) {
    drush_set_error(__FUNCTION__, 'Did not install githooks correctly. Check your permissions and try again.');
  }
  else {
    $root = drush_get_option('bootstrap-root', '');
    if ($root) {
      $root = "-r " . $root;
    }
    $root = preg_quote($root, '/');
    drush_op_system("sed -i -e's/%root/$root/g' $hooks_directory/*");
  }

  // File operations return "1" on failure and "0" on success, therefore we reverse for better logic.
  return !$code;
}

/**
 * Backup the original git hooks directory before installing drush githooks.
 *
 * @param $path
 *  Path to the current git repository.
 */
function githooks_backup($path) {
  $hooks_dir = $path . '/hooks';
  if (is_dir($hooks_dir . '.backup') && $continue = drush_confirm("hooks.backup already exists. Performing this action will override previous backups. Are you sure you want to do this?")) {
    drush_op_system("rm -rf $hooks_dir.backup");
  }
  elseif (isset($continue) && !$continue) {
    return FALSE;
  }
  drush_op_system("cp -a $hooks_dir $hooks_dir.backup");
  return TRUE;
}

/**
 * Restore the git hooks directory in place before drush githooks.
 *
 * @param $path
 *  Path to the current git repository.
 */
function githooks_restore($path) {
  $hooks_dir = $path . '/hooks';
  $backup_dir = $hooks_dir . '.backup';

  if (!is_dir($hooks_dir . '.backup')) {
    return drush_set_error(__FUNCTION__, "No hooks.backup present to restore from");
  }
  drush_op_system("rm -rf $hooks_dir");
  drush_op_system("mv $backup_dir $hooks_dir");
  return TRUE;
}

/**
 * Get a list of all git events.
 */
function githooks_events() {
  static $events;

  if (empty($events)) {
    $events = array(
      'applypatch-msg' => 'applypatch-msg',
      'commit-msg' => 'commit-msg',
      'post-applypatch' => 'post-applypatch',
      'post-checkout' => 'post-checkout',
      'post-commit' => 'post-commit',
      'post-merge' => 'post-merge',
      'post-receive' => 'post-receive',
      'post-rewrite' => 'post-rewrite',
      'post-update' => 'post-update',
      'pre-applypatch' => 'pre-applypatch',
      'pre-auto-gc' => 'pre-auto-gc',
      'pre-commit' => 'pre-commit',
      'prepare-commit-msg' => 'prepare-commit-msg',
      'pre-rebase' => 'pre-rebase',
      'pre-receive' => 'pre-receive',
      'update' => 'update',
    );
    $blacklist = drush_get_option_list('event-blacklist');
    $events = array_diff($events, $blacklist);
  }
  return $events;
}

