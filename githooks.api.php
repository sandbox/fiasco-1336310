\<?php

/**
 * @file
 *  Git Hook Plugin API.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Implementation of hook_githooks_event_EVENT.
 *
 * Invalidate a command by using drush_set_error() at any time.
 *
 * @param $event
 *  The active git event.
 * @param $arguments
 *  Arguments passed in by git related to the current operation.
 */
function hook_githooks_event_post_update($branch_ref) {
}

function hook_githooks_event_update($branch_ref, $pre_update_hash, $post_update_hash) {
}

function hook_githooks_event_pre_receive() {
  $account = $_SERVER['USER'];
  // Perform access check.
  // call drush_set_error to deny access.
}

/**
 * @} End of "addtogroup hooks".
 */
